module SessionsHelper

  # 渡されたユーザーでログインする
  def log_in(user)
    session[:user_id] = user.id
  end

  # ユーザーのセッションを永続的にする
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  # 渡されたユーザーがログイン済みユーザーであればtrueを返す
  def current_user?(user)
    user == current_user
  end

  # 現在ログイン中のユーザーを返す (いる場合)
  # def current_user
  #   current_user
  # end

  # ユーザーがログインしていればtrue、その他ならfalseを返す
  def logged_in?
    p user_signed_in?
    user_signed_in?
  end

  # 現在のユーザーをログアウトする
  def log_out
    sign_out
  end
end
