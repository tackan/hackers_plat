module MoviesHelper
  def comments_all
    @comments = @movie.comments.all
  end

  def comments_20
    @comments = @movie.comments.limit(20)
  end

  def return_button_html
    "<button class=\"btn btn-info\" type=\"button\" onclick=\"setTime(__nested_field_for_replace_with_index__)\">現在の動画のタイミングをセット</button>"

  end
end
