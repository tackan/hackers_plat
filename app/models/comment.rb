class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :movie
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :movie_id, presence: true
  validates :text, presence: true, length: { maximum:  300 }
end
