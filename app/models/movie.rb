class Movie < ApplicationRecord
  acts_as_taggable
  belongs_to :user
  has_many :comments, :dependent => :destroy
  has_many :telops, :dependent => :destroy
  accepts_nested_attributes_for :telops, allow_destroy: true
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :title, presence: true, length: { maximum: 60 }
  validates :comment, presence: true, length: { maximum: 200 }
  validates :content_path, presence: true, length: { is: 11 }

end
