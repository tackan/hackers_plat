class Telop < ApplicationRecord
  belongs_to :movie
  validates :time, presence: true, :uniqueness => {:scope => :movie_id}
  validates :text, presence: true, length: {maximum: 200}
end
