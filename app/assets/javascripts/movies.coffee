# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

player = null
isready = false
done = false
content_path = ""

onPageLoad ['movies#new','movies#create'], ->
  player = null
  head = false
  timer = null
  tag = document.createElement('script')
  tag.src = "https://www.youtube.com/iframe_api"
  firstScriptTag = document.getElementsByTagName('script')[0]
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag)
  $('#movie-tags').tagit
    fieldName:     'movie[tag_list]'
    singleField:   true
    availableTags: gon.available_tags

  $('#seek-bar').slider
    value:0,
    min:0,
    max:200,
    step:1,
    range: "min"


  @showTelopEditor = () ->

    if isready
      document.getElementById("telop-editer").style.display="block"
      document.getElementById("show").style.display="none"
      document.getElementById("showed").style.display="block"
      content_path = document.getElementById("movie_content_path").value
      content_path = content_path.replace("http://www.youtube.com/watch?v=", "")
      content_path = content_path.replace("https://www.youtube.com/watch?v=", "")
      content_path = content_path.replace(/&t=.*/, "")
      console.log(content_path)
      player = new YT.Player('player-for-telop', {
        height: '360',
        width: '640',
        videoId: content_path,
        playerVars: { 'controls': 0, 'rel': 0 },
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        }
      })

  @hideTelopEditor = () ->
    document.getElementById("telop-editer").style.display="none"
    document.getElementById("show").style.display="block"
    document.getElementById("showed").style.display="none"
    player.destroy()


  @onYouTubeIframeAPIReady = () ->
    console.log("ready")
    isready = true

  @onPlayerReady = (event) ->
    console.log("onready")
    d = player.getDuration()
    $('#seek-bar').slider
      value:0,
      min:0,
      max:d,
      step:1,
      range: "min",
      change: (evt,ui) ->
      ,
      slide: (evt,ui) ->
        setTimerLabel(ui.value)
        if player.getPlayerState() == 1
          player.seekTo(ui.value)
        else if player.getPlayerState() == 5
          head = true
          player.playVideo()
        else
          player.seekTo(ui.value)
          player.pauseVideo()
    str = document.getElementById('movie_telops_template').innerHTML.toString()
    str = str.replace("max=\"12345\"", "max=\"" + d + "\"")
    str = str.replace("disabled=\"disabled\"", "")
    document.getElementById('movie_telops_template').innerHTML = str
    elements = document.getElementsByClassName('number-field')
    for i,value of elements
      value.max = d
      value.disabled = false



  @onPlayerStateChange = (event) ->
    if head
      if player.getPlayerState() == 1
        head = false
        player.pauseVideo()
    else
      if player.getPlayerState() == 1
        timer = setInterval("setCurrentToSeek()", 10)
      else
        if timer
          clearInterval(timer)

  @setCurrentToSeek = () ->
    current = player.getCurrentTime()
    $('#seek-bar').slider('value', current)
    setTimerLabel(current)

  @setTimerLabel = (current) ->
    document.getElementById('timer').innerHTML = Math.floor(current)

  @stopVideo = () ->
    player.stopVideo()

  @setTime = (index) ->
    document.getElementById('movie_telops_attributes_' + index + '_time').value = Math.floor(player.getCurrentTime())

onPageLoad ['movies#edit','movies#update'], ->
  player = null
  head = false
  timer = null
  tag = document.createElement('script')
  tag.src = "https://www.youtube.com/iframe_api"
  firstScriptTag = document.getElementsByTagName('script')[0]
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag)
  console.log('edit')
  $('#movie-tags').tagit
    fieldName:     'movie[tag_list]'
    singleField:   true
    availableTags: gon.available_tags

  if gon.movie_tags?
    $('#movie-tags').tagit 'removeAll'
    for tag in gon.movie_tags
      $('#movie-tags').tagit 'createTag', tag

  $('#seek-bar').slider
    value:0,
    min:0,
    max:200,
    step:1,
    range: "min"

  @tag_it = () ->
    console.log('tag')
    $('#movie-tags').tagit
    fieldName:     'movie[tag_list]'
    singleField:   true
    availableTags: gon.available_tags

  content_path = document.getElementById("movie_content_path").value
  content_path = content_path.replace("http://www.youtube.com/watch?v=", "")
  content_path = content_path.replace("https://www.youtube.com/watch?v=", "")
  content_path = content_path.replace(/&t=.*/, "")

  @showTelopEditor = () ->

    if isready
      document.getElementById("telop-editer").style.display="block"
      document.getElementById("show").style.display="none"
      document.getElementById("showed").style.display="block"
      player = new YT.Player('player-for-telop', {
        height: '360',
        width: '640',
        videoId: content_path,
        playerVars: { 'controls': 0, 'rel': 0 },
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        }
      })


  @hideTelopEditor = () ->
    document.getElementById("telop-editer").style.display="none"
    document.getElementById("show").style.display="block"
    document.getElementById("showed").style.display="none"
    player.destroy()
    player = null


  @onYouTubeIframeAPIReady = () ->
    isready = true
    console.log('isready')

  @onPlayerReady = (event) ->
    console.log("onready")
    d = player.getDuration()
    console.log(d)
    $('#seek-bar').slider
      value:0,
      min:0,
      max:d,
      step:1,
      range: "min",
      change: (evt,ui) ->
      ,
      slide: (evt,ui) ->
        setTimerLabel(ui.value)
        if player.getPlayerState() == 1
          player.seekTo(ui.value)
        else if player.getPlayerState() == 5
          head = true
          player.playVideo()
        else
          player.seekTo(ui.value)
          player.pauseVideo()

    str = document.getElementById('movie_telops_template').innerHTML.toString()
    str = str.replace("max=\"12345\"", "max=\"" + d + "\"")
    str = str.replace("disabled=\"disabled\"", "")
    document.getElementById('movie_telops_template').innerHTML = str
    elements = document.getElementsByClassName('number-field')
    for i,value of elements
      value.max = d
      value.disabled = false

  @onPlayerStateChange = (event) ->
    if head
      if player.getPlayerState() == 1
        head = false
        player.pauseVideo()
    else
      if player.getPlayerState() == 1
        timer = setInterval("setCurrentToSeek()", 10)
      else
        if timer
          clearInterval(timer)


  @setCurrentToSeek = () ->
    $('#seek-bar').slider('value', player.getCurrentTime())
    console.log(document.getElementById('timer'))
    document.getElementById('timer').innerHTML = Math.floor(player.getCurrentTime())

  @setTimerLabel = (current) ->
    document.getElementById('timer').innerHTML = Math.floor(current)

  @stopVideo = () ->
    player.stopVideo()

  @setTime = (index) ->
    document.getElementById('movie_telops_attributes_' + index + '_time').value = Math.floor(player.getCurrentTime())

onPageLoad 'movies#index', ->
  $('#movie-tags').tagit
    fieldName:     'movie[tag_list]'
    singleField:   true
    availableTags: gon.available_tags

onPageLoad 'movies#show', ->
  player = null
  head = false
  timer = null
  content_path = gon.content_path
  telops = gon.telops
  console.log(telops)
  tag = document.createElement('script')
  tag.src = "https://www.youtube.com/iframe_api"
  firstScriptTag = document.getElementsByTagName('script')[0]
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag)

  $('#seek-bar').slider
    value:0,
    min:0,
    max:200,
    step:1,
    range: "min"

  if isready
    console.log('isready')
    player = new YT.Player('player', {
      height: '450',
      width: '800',
      videoId: content_path,
      playerVars: { 'controls': 0, 'rel': 0 },
      events: {
        'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange
      }
    })

  @urlAutoLink = (text) ->
    return text.replace(/(http:\/\/[\x21-\x7e]+)/gi, "<a href='$1' target='_blank'>$1</a>").replace(/(https:\/\/[\x21-\x7e]+)/gi, "<a href='$1' target='_blank'>$1</a>")


  @onYouTubeIframeAPIReady = () ->
    console.log("ready")
    isready = true
    player = new YT.Player('player', {
        height: '450',
        width: '800',
        videoId: content_path,
        playerVars: { 'controls': 0, 'rel': 0 },
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        }
      })

  @onPlayerReady = (event) ->
    console.log("onready")
    d = player.getDuration()
    $('#seek-bar').slider
      value:0,
      min:0,
      max:d,
      step:1,
      range: "min",
      change: (evt,ui) ->
      ,
      slide: (evt,ui) ->
        console.log('ui.value')

        showTelop(ui.value)
        setTimerLabel(ui.value)
        if player.getPlayerState() == 1
          player.seekTo(ui.value)
        else if player.getPlayerState() == 5
          head = true
          player.playVideo()
        else
          player.seekTo(ui.value)
          player.pauseVideo()


  @onPlayerStateChange = (event) ->
    if head
      if player.getPlayerState() == 1
        head = false
        player.pauseVideo()
    else
      if player.getPlayerState() == 1
        timer = setInterval("setCurrentToSeek()", 10)
      else
        if timer
          clearInterval(timer)

  @setCurrentToSeek = () ->
    current = player.getCurrentTime()
    $('#seek-bar').slider('value', current)
    setTimerLabel(current)
    showTelop(current)

  @setTimerLabel = (current) ->
    document.getElementById('timer').innerHTML = Math.floor(current)

  @stopVideo = () ->
    player.stopVideo()

  @showTelop = (current) ->
    for i,telop of telops
      if telop.time <= current
        document.getElementById('user-telop').innerHTML = urlAutoLink(telop.text.replace(/\r?\n/g, '<br>'))
        break

