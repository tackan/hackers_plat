class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include ApplicationHelper
  include SessionsHelper
  include MoviesHelper

  before_filter :configure_permitted_parameters, if: :devise_controller?

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
      devise_parameter_sanitizer.permit(:account_update, keys: [:name])
    end

  private

    # ログイン済みユーザーかどうか確認
    def logged_in_user
      unless logged_in?
        flash.notice = "まずはログインしてください。"
        redirect_to login_path
      end
    end
end
