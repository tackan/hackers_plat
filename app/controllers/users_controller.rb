class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :destroy, :current]
  # before_action :correct_user,   only: [:edit, :update, :destroy]
  # before_action :not_logged_in_user,   only: [:new, :create]
  # before_action :admin_user,     only: :destroy
  # def index
  # end

  def show
    @user = User.find(params[:id])
    redirect_to root_url and return unless @user.confirmed?
  end

  def current
    @movies = current_user.movies.paginate(page: params[:page], per_page: 30)
    render 'profile'
  end

  # def new
  # end

  # def create
  # end

  # def edit
  # end

  # def update
  #   @user = User.find(params[:format])
  #   if @user.update_attributes(user_params)
  #     # 更新に成功した場合を扱う。
  #      flash[:success] = "編集完了"
  #     redirect_to @user
  #   else
  #     render 'edit'
  #   end
  # end

  # def destroy
  #   User.find(params[:id]).destroy
  #   flash[:success] = "User deleted"
  #   redirect_to users_url
  # end

  # private

  # def user_params
  #   params.require(:user).permit(:name, :email, :password,
  #                                :password_confirmation)
  # end

  # # ログイン済みユーザーかどうか確認
  # def logged_in_user
  #   unless logged_in?
  #     flash[:warning] = "まずはログインしてください。"
  #     redirect_to login_url
  #   end
  # end

  # # ログイン済みでないユーザーかどうか確認
  # def not_logged_in_user
  #   if logged_in?
  #     redirect_to root_url
  #   end
  # end

  # # 正しいユーザーかどうか確認
  # def correct_user
  #   @user = User.find(params[:id])
  #   redirect_to(root_url) unless current_user?(@user)
  # end

  # # 管理者かどうか確認
  # def admin_user
  #   redirect_to(root_url) unless current_user.admin?
  # end
end
