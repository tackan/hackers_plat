class OmniauthCallbacksController < ApplicationController
  def twitter
    @user = User.from_omniauth(request.env["omniauth.auth"].except("extra"))
    if !@user.email.empty?
      flash.notice = "ログインしました！"
      @user.skip_confirmation!
      sign_in @user
      redirect_to root_url
    else
      session["devise.user_attributes"] = @user.attributes
      redirect_to new_user_registration_url
    end
  end
end
