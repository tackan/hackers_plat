class MoviesController < ApplicationController
  require 'movie_form_builder'
  require 'telop_form_builder'
  # include CustomFormBuilder

  before_action :logged_in_user, only: [:new, :create, :update, :destroy, :create_comment]
  before_action  :set_movie_tags_to_gon, only: [:edit,:update]
  before_action  :set_available_tags_to_gon, only: [:index, :new, :create, :edit, :update]
  before_action :current_user?, only: [:update, :destroy]
  @@movie = nil
  def index
  @movies = params[:tag].present? ? Movie.tagged_with(params[:tag].split(","), any: or?(params[:condition])) : Movie.all
  @movies = @movies.paginate(page: params[:page], per_page: 30)
  end

  def show
    @movie = Movie.find(params[:id])
    set_movie_telop_and_path_to_gon(@movie)
    @@movie = @movie
    @comments = comments_all # ほんとは最初の20件表示とすべて表示で切り替えたいけど暫定的に全部取得
    @comment = @movie.comments.new
    @count = @movie.comments.count
  end

  def new
    @movie = Movie.new
  end

  def create
    @before_path
    @movie = current_user.movies.build(movie_params)
    if @movie.save
      flash.notice = '投稿しました。'
      redirect_to root_url
    else
      @movie[:content_path] = @before_path
      p @before_path
      render 'new'
    end
  end

  def edit
    @movie = Movie.find(params[:id])
    @movie[:content_path] = 'https://www.youtube.com/watch?v=' + @movie[:content_path]
  end

  def update
    @before_path
    @movie = Movie.find(params[:id])
    if @movie.update_attributes(movie_params)
      flash.notice = '編集が完了しました。'
      redirect_to current_user_path
    else
      @movie[:content_path] = 'https://www.youtube.com/watch?v=' + @movie[:content_path]
      render 'edit'
    end
  end

  def destroy
  end

  def create_comment
    @comment = @@movie.comments.build(comment_params)
    @comment.user_id = current_user.id
    if @comment.save
      flash.notice = 'コメントを投稿しました。'
      redirect_to action: 'show', id: @@movie.id
    else
      flash.alert = 'エラー。コメントを投稿できませんでした。'
      redirect_to action: 'show', id: @@movie.id
    end
  end

  private

    def movie_params
      strong_params = params.require(:movie).permit(:title, :comment, :content_path, :tag_list, telops_attributes: [:id, :movie_id, :text, :time, :_destroy])
      if strong_params[:telops_attributes]
        p 'MOVIE PARAMS'
        p strong_params
        strong_params[:telops_attributes] = strong_params[:telops_attributes].delete_if { |key, value| (!value[:time].nil? && value[:time].empty? && value[:text].empty?) || (value[:time].nil? && value[:text.empty?])}
      end
      @before_path = strong_params[:content_path].clone
      strong_params[:content_path] = trim_path(strong_params[:content_path])
      strong_params
    end

    def comment_params
      params.require(:comment).permit(:text)
    end

    def set_movie_tags_to_gon
      if current_user.movies.exists?(id: params[:id])
        @movie = current_user.movies.find(params[:id])
        gon.movie_tags = @movie.tag_list
        p @movie.tag_list
      else
        redirect_to root_url
      end
    end

    def set_available_tags_to_gon
      gon.available_tags = Tag.all.pluck(:name)
    end

    def set_movie_telop_and_path_to_gon(movie)
      gon.content_path = movie.content_path
      gon.telops = movie.telops.order(time: :DESC)
    end

    def current_user?
      redirect_to root_url unless current_user.movies.exists?(id: params[:id])
    end

    def trim_path(path)
      http_path = /#{Regexp.escape("http://www.youtube.com/watch?v=")}/
      https_path = /#{Regexp.escape("https://www.youtube.com/watch?v=")}/
      t_path = /&t=.*/
      path.slice!(http_path)
      path.slice!(https_path)
      path.slice!(t_path)
      path
    end

end
